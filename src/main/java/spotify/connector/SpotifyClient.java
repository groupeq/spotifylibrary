package spotify.connector;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import spotify.connector.dto.AboutMeDTO;

import java.net.URI;

@Component
@Log4j2
public class SpotifyClient extends ApiAbstraction {
    private static final String SPOTIFY_URI_ABOUT_ME = "https://api.spotify.com/v1/me";
    private final AuthHandler authHandler;

    public SpotifyClient(AuthHandler authHandler) {
        this.authHandler = authHandler;
    }


    public AboutMeDTO getUserInfo() {
        log.debug("spotifyClieny getuserInfo");
        return this.restTemplate.getForObject(SPOTIFY_URI_ABOUT_ME, AboutMeDTO.class);
    }

    public URI getAuthorizationUri() {
        log.debug("spotifyClient getAuthorizationUri");
        return this.authHandler.getAuthorizationUri();
    }

    public void authorizationCallback(String code) {
        log.debug("spotifyClient getAuthorizationcallback");
        this.authHandler.authorizationCallback(code);
    }
}