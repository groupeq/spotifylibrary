package spotify.connector;

import org.springframework.stereotype.Service;
import spotify.connector.dto.AboutMeDTO;

@Service
public class UserService {
    private final SpotifyClient spotifyClient;

    public UserService(SpotifyClient spotifyClient) {
        this.spotifyClient = spotifyClient;
    }

    public AboutMeDTO getUserInfo() {
        return spotifyClient.getUserInfo();
    }
}