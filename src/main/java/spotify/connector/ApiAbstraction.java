package spotify.connector;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

public abstract class ApiAbstraction {
    protected RestTemplate restTemplate;

    public ApiAbstraction() {
//        this.restTemplate = new RestTemplate();
//        if (userInformation != null) {
//            this.restTemplate.getInterceptors()
//                    .add(getBearerTokenInterceptor(userInformation.getAuthorizationResponseDTO().getToken()));
//        } else {
//            this.restTemplate.getInterceptors().add(getNoTokenInterceptor());
//        }
    }

    private ClientHttpRequestInterceptor getBearerTokenInterceptor(String accessToken) {
        return (request, bytes, execution) -> {
            request.getHeaders().add("Authorization", "Bearer " + accessToken);
            return execution.execute(request, bytes);
        };
    }

    private ClientHttpRequestInterceptor getNoTokenInterceptor() {
        return (request, bytes, execution) -> {
            throw new IllegalStateException(
                    "Can't access the API without an access token");
        };
    }
}