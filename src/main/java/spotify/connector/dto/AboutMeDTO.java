package spotify.connector.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AboutMeDTO {
    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("country")
    private String country;
    @JsonProperty("email")
    private String email;

    public AboutMeDTO() {
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toString() {
        return "AboutMeDTO{displayName='" + this.displayName + "', country='" + this.country + "', email='" + this.email + "'}";
    }
}