package spotify.connector.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class UserAuthorizationResponseDTO implements Serializable {
    private static final long serialVersionUID = -1312312312312L;
    @JsonProperty("access_token")
    private String token;
    @JsonProperty("token_type")
    private String tokenType;
    @JsonProperty("expires_in")
    private long expiresIn;
    @JsonProperty("refresh_token")
    private String refreshToken;
    @JsonProperty("scope")
    private String scope;

    public UserAuthorizationResponseDTO() {
    }

    public String getToken() {
        return this.token;
    }

    public String getTokenType() {
        return this.tokenType;
    }

    public long getExpiresIn() {
        return this.expiresIn;
    }

    public String getRefreshToken() {
        return this.refreshToken;
    }

    public String getScope() {
        return this.scope;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String toString() {
        return "UserAuthorizationResponseDTO{token='" + this.token + "', tokenType='" + this.tokenType + "', expiresIn=" + this.expiresIn + ", refreshToken='" + this.refreshToken + "', scope='" + this.scope + "'}";
    }
}