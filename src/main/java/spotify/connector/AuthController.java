package spotify.connector;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
public class AuthController extends AbstractApiController {
    private final SpotifyClient spotifyClient;

    public AuthController(SpotifyClient spotifyClient) {
        this.spotifyClient = spotifyClient;
    }

    @GetMapping("login")
    @ResponseBody
    public String spotifyLogin() {
        log.debug("AuthController spotify Login");
        return spotifyClient.getAuthorizationUri().toString();
    }

    @GetMapping("callback")
    @ResponseStatus(value = HttpStatus.OK)
    public void getCallback(@RequestParam("code") String code) {
        log.debug("AuthController getCallback");
        log.debug("Handling authorization callback with code: {}", code);
        spotifyClient.authorizationCallback(code);
    }
}