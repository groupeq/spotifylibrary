package spotify.connector;

import com.fasterxml.jackson.core.JsonProcessingException;

public class ExceptionToChangeName extends Exception {
    public ExceptionToChangeName(JsonProcessingException e) {
        super(e);
    }

    public ExceptionToChangeName(Throwable cause) {
        super(cause);
    }
}
