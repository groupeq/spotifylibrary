package spotify.connector;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import spotify.connector.dto.UserAuthorizationResponseDTO;

import java.io.Serializable;
import java.net.URI;
import java.util.Arrays;

import static spotify.connector.SpotifyScope.USER_READ_EMAIL;
import static spotify.connector.SpotifyScope.USER_READ_PRIVATE;

@Service
@Log4j2
public class AuthService implements Auth, Serializable {
    private static final long serialVersionUID = 112412512412L;
    private final AuthUtils authUtils;
    private final UserAuthRepository userAuthRepository;

    public AuthService(AuthUtils authUtils, UserAuthRepository userAuthRepository) {
        this.authUtils = authUtils;
        this.userAuthRepository = userAuthRepository;
    }

    @Override
    public URI getAuthorizationUri() {
        String authorizationUrl = getCreatedUrl();
        return URI.create(authorizationUrl);
    }

    private String getCreatedUrl() {
        SpotifyAuthData spotifyAuthData = getSpotifyAuthData();
        return SpotifyAuthData.authorizationUrl(spotifyAuthData);
    }

    @Override
    public void authorizationCallback(String code) {
        authUtils.makeAGetCall(code);
    }


    private SpotifyAuthData getSpotifyAuthData() {
        return new SpotifyAuthData
                .Builder()
                .setClientId(authUtils.clientId)
                .setResponseType("code")
                .setUrl("https://accounts.spotify.com/authorize?")
                .setRedirectUri(URI.create(AuthUtils.URI_AUTHORIZATION_CALLBACK))
                .setState(RandomStringUtils.randomAlphabetic(1, 16))
                .setScope(Arrays.asList(USER_READ_PRIVATE, USER_READ_EMAIL))
                .build();
    }
}