package spotify.connector.app;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class ApplicationEnvironment {
    private final Environment environment;

    public ApplicationEnvironment(Environment environment) {
        this.environment = environment;
    }

    public String getClientId() {
        return environment.getProperty("spotify.client.id");
    }

    public String getClientSecretNumber() {
        return environment.getProperty("spotify.client.secret.id");
    }
}
