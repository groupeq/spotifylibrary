package spotify.connector;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.net.URI;

@Log4j2
@Service
public class AuthHandler implements Auth, Serializable {

    private final AuthService authService;

    public AuthHandler(AuthService authService) {
        this.authService = authService;
    }

    @Override
    public URI getAuthorizationUri() {
        return authService.getAuthorizationUri();
    }

    @Override
    public void authorizationCallback(String code) {
        authService.authorizationCallback(code);
    }
}