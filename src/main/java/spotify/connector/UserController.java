package spotify.connector;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import spotify.connector.dto.AboutMeDTO;

@Log4j2
@RestController
public class UserController extends AbstractApiController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("user-details")
    public AboutMeDTO getUserDetails() {
        log.debug("User-details controller");
        AboutMeDTO userInfo = userService.getUserInfo();
        log.info(userInfo);
        return userInfo;
    }
}