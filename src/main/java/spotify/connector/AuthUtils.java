package spotify.connector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import spotify.connector.app.ApplicationEnvironment;
import spotify.connector.dto.UserAuthorizationResponseDTO;

import java.util.Base64;

@Component
public class AuthUtils {
    private static final Logger log = LogManager.getLogger(AuthUtils.class);
    public static final String SPOTIFY_URI_TOKEN = "https://accounts.spotify.com/api/token";
    public static final String URI_AUTHORIZATION_CALLBACK = "http://localhost:8080/api/v1/callback";
    public final String clientId;
    public final String clientSecretId;

    public AuthUtils(ApplicationEnvironment applicationEnvironment) {
        this.clientId = applicationEnvironment.getClientId();
        this.clientSecretId = applicationEnvironment.getClientSecretNumber();
    }

    public Flux<UserAuthorizationResponseDTO> makeAGetCall(String code) {
        Flux<UserAuthorizationResponseDTO> userAuthorizationResponseDTOFlux =
                WebClient.create()
                        .post()
                        .uri(SPOTIFY_URI_TOKEN)
                        .body(BodyInserters.fromFormData("grant_type", "authorization_code").with("code", code).
                                with("redirect_uri", URI_AUTHORIZATION_CALLBACK))
                        .headers((httpHeaders) -> httpHeaders.add("Authorization", "Basic " + this.getEncodedSpotifyAccess()))
                        .exchangeToFlux((clientResponse) -> clientResponse.bodyToFlux(UserAuthorizationResponseDTO.class));

        userAuthorizationResponseDTOFlux.subscribe((auth) -> log.info(auth.toString()));
        return userAuthorizationResponseDTOFlux;
    }

    private String getEncodedSpotifyAccess() {
        String s = this.clientId + ":" + this.clientSecretId;
        return Base64.getEncoder().encodeToString(s.getBytes());
    }
}