package spotify.connector;

import java.net.URI;

public interface Auth {
    URI getAuthorizationUri();

    void authorizationCallback(String code) throws ExceptionToChangeName;
}