FROM java:8-jdk-alpine
COPY ./target/SpotifyLibrary-1.0-SNAPSHOT.war /usr/app/
WORKDIR /usr/app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "SpotifyLibrary-1.0-SNAPSHOT.war"]